json.array!(@users) do |user|
  json.extract! user, :red, :yellow, :blue, :gray
  json.url user_url(user, format: :json)
end
