json.array!(@cycles) do |cycle|
  json.extract! cycle, :initial_cash, :actual_cash, :paid
  json.url cycle_url(cycle, format: :json)
end
