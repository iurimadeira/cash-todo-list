class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :description
      t.timestamp :deadline
      t.string :importance
      t.boolean :success

      t.timestamps
    end
  end
end
