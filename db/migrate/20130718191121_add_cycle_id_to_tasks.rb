class AddCycleIdToTasks < ActiveRecord::Migration
  def change
    add_column :tasks, :cycle_id, :integer
  end
end
