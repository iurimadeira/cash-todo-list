class CreateCycles < ActiveRecord::Migration
  def change
    create_table :cycles do |t|
      t.decimal :initial_cash
      t.decimal :actual_cash
      t.boolean :paid

      t.timestamps
    end
  end
end
