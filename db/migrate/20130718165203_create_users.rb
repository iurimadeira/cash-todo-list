class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.decimal :red
      t.decimal :yellow
      t.decimal :blue
      t.decimal :gray

      t.timestamps
    end
  end
end
